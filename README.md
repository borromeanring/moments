# Moments

A collection of my favorite moments from various media I have consumed and my comments on them.

<!-- Start of Index -->
# Index by Medium
- #### Manga
  - #### Haikyu!!
    - #### S
      - [314-Soar](Manga/Haikyu!!/314-Soar.md)
    - #### A
      - [331-Protagonists](Manga/Haikyu!!/331-Protagonists.md)
  - #### One-Piece
    - #### S
      - [1049-Luffy's-World](Manga/One-Piece/1049-Luffy's-World.md)
      - [1055-New-Age](Manga/One-Piece/1055-New-Age.md)
  - #### Vagabond
    - #### S
      - [21-Darkness](Manga/Vagabond/21-Darkness.md)
      - [35-To-Truly-See](Manga/Vagabond/35-To-Truly-See.md)
      - [36-Awareness-And-Acceptance](Manga/Vagabond/36-Awareness-And-Acceptance.md)
      - [39-Perceiving-True-Strength](Manga/Vagabond/39-Perceiving-True-Strength.md)
      - [98-103-Invincible](Manga/Vagabond/98-103-Invincible.md)
      - [109-The-View-From-Top](Manga/Vagabond/109-The-View-From-Top.md)
      - [129-A-Fateful-Encounter](Manga/Vagabond/129-A-Fateful-Encounter.md)
      - [131-132-A-Deaf-Child-and-A-Blind-Father](Manga/Vagabond/131-132-A-Deaf-Child-and-A-Blind-Father.md)
      - [223-Reflection](Manga/Vagabond/223-Reflection.md)
      - [224-Truly-Strong-People](Manga/Vagabond/224-Truly-Strong-People.md)
      - [274-Advancing-Down-One-Path](Manga/Vagabond/274-Advancing-Down-One-Path.md)
      - [301-Initial-Form](Manga/Vagabond/301-Initial-Form.md)
      - [311-The-Meaning-Of-Strength](Manga/Vagabond/311-The-Meaning-Of-Strength.md)
      - [315-A-Dying-Village](Manga/Vagabond/315-A-Dying-Village.md)
      - [317-The-Value-Of-Life](Manga/Vagabond/317-The-Value-Of-Life.md)
      - [317-Being-Free](Manga/Vagabond/317-Being-Free.md)
    - #### A
      - [19-A-Selfish-Death](Manga/Vagabond/19-A-Selfish-Death.md)
      - [63-Bones](Manga/Vagabond/63-Bones.md)
      - [122-Hatred](Manga/Vagabond/122-Hatred.md)
      - [131-132-An-Odd-Child](Manga/Vagabond/131-132-An-Odd-Child.md)
      - [169-Fear-And-Strength](Manga/Vagabond/169-Fear-And-Strength.md)
      - [171-172-A-Conversation-With-A-Deaf-Person](Manga/Vagabond/171-172-A-Conversation-With-A-Deaf-Person.md)
      - [236-Soft-Air](Manga/Vagabond/236-Soft-Air.md)
      - [279-280-Unrivaled-Under-The-Heavens](Manga/Vagabond/279-280-Unrivaled-Under-The-Heavens.md)
      - [280-281-The-Element-Of-Surprise](Manga/Vagabond/280-281-The-Element-Of-Surprise.md)
      - [281-Answers](Manga/Vagabond/281-Answers.md)
      - [288-The-You-Of-Today](Manga/Vagabond/288-The-You-Of-Today.md)
      - [294-A-Kind-Person](Manga/Vagabond/294-A-Kind-Person.md)
      - [302-Flowing-Like-Water](Manga/Vagabond/302-Flowing-Like-Water.md)
      - [303-Heaven](Manga/Vagabond/303-Heaven.md)
      - [306-The-Dead](Manga/Vagabond/306-The-Dead.md)
      - [322-A-Nice-Smile](Manga/Vagabond/322-A-Nice-Smile.md)
    - #### B
      - [62-What-Am-I](Manga/Vagabond/62-What-Am-I.md)
      - [66-Island](Manga/Vagabond/66-Island.md)
      - [70-Vision](Manga/Vagabond/70-Vision.md)
      - [76-To-Experience](Manga/Vagabond/76-To-Experience.md)
      - [129-An-Odd-One](Manga/Vagabond/129-An-Odd-One.md)
      - [133-An-Odd-Child-With-A-Mighty-Sword](Manga/Vagabond/133-An-Odd-Child-With-A-Mighty-Sword.md)
      - [227-The-Future-And-The-Past](Manga/Vagabond/227-The-Future-And-The-Past.md)
      - [257-Path](Manga/Vagabond/257-Path.md)
      - [305-Water](Manga/Vagabond/305-Water.md)
      - [306-Infinite](Manga/Vagabond/306-Infinite.md)
  - #### Vinland-Saga
    - #### S
      - [7-No-Enemies](Manga/Vinland-Saga/7-No-Enemies.md)
      - [52-53-Saving-Wales-and-Canute](Manga/Vinland-Saga/52-53-Saving-Wales-and-Canute.md)
      - [54-The-Dagger](Manga/Vinland-Saga/54-The-Dagger.md)
      - [54-True-Battle](Manga/Vinland-Saga/54-True-Battle.md)
      - [68-Being-Empty](Manga/Vinland-Saga/68-Being-Empty.md)
      - [154-Valhalla](Manga/Vinland-Saga/154-Valhalla.md)
    - #### B
      - [160-Ties-with-War](Manga/Vinland-Saga/160-Ties-with-War.md)

# Index by Tier
- #### S
  - #### Manga
    - #### Haikyu!!
      - [314-Soar](Manga/Haikyu!!/314-Soar.md)
    - #### One-Piece
      - [1049-Luffy's-World](Manga/One-Piece/1049-Luffy's-World.md)
      - [1055-New-Age](Manga/One-Piece/1055-New-Age.md)
    - #### Vagabond
      - [21-Darkness](Manga/Vagabond/21-Darkness.md)
      - [35-To-Truly-See](Manga/Vagabond/35-To-Truly-See.md)
      - [36-Awareness-And-Acceptance](Manga/Vagabond/36-Awareness-And-Acceptance.md)
      - [39-Perceiving-True-Strength](Manga/Vagabond/39-Perceiving-True-Strength.md)
      - [98-103-Invincible](Manga/Vagabond/98-103-Invincible.md)
      - [109-The-View-From-Top](Manga/Vagabond/109-The-View-From-Top.md)
      - [129-A-Fateful-Encounter](Manga/Vagabond/129-A-Fateful-Encounter.md)
      - [131-132-A-Deaf-Child-and-A-Blind-Father](Manga/Vagabond/131-132-A-Deaf-Child-and-A-Blind-Father.md)
      - [223-Reflection](Manga/Vagabond/223-Reflection.md)
      - [224-Truly-Strong-People](Manga/Vagabond/224-Truly-Strong-People.md)
      - [274-Advancing-Down-One-Path](Manga/Vagabond/274-Advancing-Down-One-Path.md)
      - [301-Initial-Form](Manga/Vagabond/301-Initial-Form.md)
      - [311-The-Meaning-Of-Strength](Manga/Vagabond/311-The-Meaning-Of-Strength.md)
      - [315-A-Dying-Village](Manga/Vagabond/315-A-Dying-Village.md)
      - [317-The-Value-Of-Life](Manga/Vagabond/317-The-Value-Of-Life.md)
      - [317-Being-Free](Manga/Vagabond/317-Being-Free.md)
    - #### Vinland-Saga
      - [7-No-Enemies](Manga/Vinland-Saga/7-No-Enemies.md)
      - [52-53-Saving-Wales-and-Canute](Manga/Vinland-Saga/52-53-Saving-Wales-and-Canute.md)
      - [54-The-Dagger](Manga/Vinland-Saga/54-The-Dagger.md)
      - [54-True-Battle](Manga/Vinland-Saga/54-True-Battle.md)
      - [68-Being-Empty](Manga/Vinland-Saga/68-Being-Empty.md)
      - [154-Valhalla](Manga/Vinland-Saga/154-Valhalla.md)
- #### A
  - #### Manga
    - #### Haikyu!!
      - [331-Protagonists](Manga/Haikyu!!/331-Protagonists.md)
    - #### Vagabond
      - [19-A-Selfish-Death](Manga/Vagabond/19-A-Selfish-Death.md)
      - [63-Bones](Manga/Vagabond/63-Bones.md)
      - [122-Hatred](Manga/Vagabond/122-Hatred.md)
      - [131-132-An-Odd-Child](Manga/Vagabond/131-132-An-Odd-Child.md)
      - [169-Fear-And-Strength](Manga/Vagabond/169-Fear-And-Strength.md)
      - [171-172-A-Conversation-With-A-Deaf-Person](Manga/Vagabond/171-172-A-Conversation-With-A-Deaf-Person.md)
      - [236-Soft-Air](Manga/Vagabond/236-Soft-Air.md)
      - [279-280-Unrivaled-Under-The-Heavens](Manga/Vagabond/279-280-Unrivaled-Under-The-Heavens.md)
      - [280-281-The-Element-Of-Surprise](Manga/Vagabond/280-281-The-Element-Of-Surprise.md)
      - [281-Answers](Manga/Vagabond/281-Answers.md)
      - [288-The-You-Of-Today](Manga/Vagabond/288-The-You-Of-Today.md)
      - [294-A-Kind-Person](Manga/Vagabond/294-A-Kind-Person.md)
      - [302-Flowing-Like-Water](Manga/Vagabond/302-Flowing-Like-Water.md)
      - [303-Heaven](Manga/Vagabond/303-Heaven.md)
      - [306-The-Dead](Manga/Vagabond/306-The-Dead.md)
      - [322-A-Nice-Smile](Manga/Vagabond/322-A-Nice-Smile.md)
- #### B
  - #### Manga
    - #### Vagabond
      - [62-What-Am-I](Manga/Vagabond/62-What-Am-I.md)
      - [66-Island](Manga/Vagabond/66-Island.md)
      - [70-Vision](Manga/Vagabond/70-Vision.md)
      - [76-To-Experience](Manga/Vagabond/76-To-Experience.md)
      - [129-An-Odd-One](Manga/Vagabond/129-An-Odd-One.md)
      - [133-An-Odd-Child-With-A-Mighty-Sword](Manga/Vagabond/133-An-Odd-Child-With-A-Mighty-Sword.md)
      - [227-The-Future-And-The-Past](Manga/Vagabond/227-The-Future-And-The-Past.md)
      - [257-Path](Manga/Vagabond/257-Path.md)
      - [305-Water](Manga/Vagabond/305-Water.md)
      - [306-Infinite](Manga/Vagabond/306-Infinite.md)
    - #### Vinland-Saga
      - [160-Ties-with-War](Manga/Vinland-Saga/160-Ties-with-War.md)
<!-- End of Index -->

# Disclaimer

I do not own this content. This content is published under Fair Use:

Copyright Disclaimer Under Section 107 of the Copyright Act in 1976; Allowance is made for "Fair Use" for purposes such as criticism, comment, news reporting, teaching, scholarship, and research.
Fair use is a use permitted by copyright statute that might otherwise be infringing. Non-profit, educational or personal use tips the balance in favor of fair use.

All rights and credit go directly to its rightful owners. No copyright infringement intended.
