#!/usr/bin/python3

import sys, subprocess, re
from collections import defaultdict

README_FILE = 'README.md'
FIND_COMMAND = 'find Manga -type f -exec grep Tier {} +'
GIT_COMMAND = f'git add {README_FILE}'
START_MARKER = '<!-- Start of Index -->'
END_MARKER = '<!-- End of Index -->'

def get_all_media():
  return subprocess.run(FIND_COMMAND.split(), capture_output=True, text=True).stdout

def get_indexes():
  def val_dict_1():
    def val_dict_2():
      def val_list():
        return []
      return defaultdict(val_list)
    return defaultdict(val_dict_2)
  return defaultdict(val_dict_1), defaultdict(val_dict_1)

def parse_all_media(all_media):
  index_by_medium, index_by_tier = get_indexes()
  pattern = r"([^/]+)/([^/]+)/([^/]+)\.md:- Tier: ([^/]+)"
  for line in all_media:
    match = re.search(pattern, line)
    if match:
      medium = match.group(1)
      title = match.group(2)
      segment = match.group(3)
      tier = match.group(4)
      path = f'{medium}/{title}/{segment}.md'
      index_by_medium[medium][title][tier].append((segment, path))
      index_by_tier[tier][medium][title].append((segment, path))
    else:
      print(f'Regex parsing failed for {line}.')
      sys.exit(1)
  return index_by_medium, index_by_tier

def pprint_markdown(obj, indent=0):
  if isinstance(obj, dict):
    result = ""
    sorted_keys = sorted(obj.keys())
    sorted_keys = sorted(sorted_keys, key=lambda x: x != 'S')
    for key in sorted_keys:
      value = obj[key]
      result += f"{'  ' * indent}- #### {key}\n"
      result += pprint_markdown(value, indent + 1)
    return result
  elif isinstance(obj, list):
    result = ""
    sorted_obj = sorted(obj, key=lambda x: int(x[0].split('-')[0]))
    for segment, path in sorted_obj:
      result += f"{'  ' * indent}- [{segment}]({path})\n"
    return result
  else:
    return ""

def replace_content_between_markers(new_content):
  with open(README_FILE, 'r') as file:
    content = file.read()
  start_pos = content.find(START_MARKER)
  end_pos = content.find(END_MARKER)
  if start_pos != -1 and end_pos != -1:
    updated_content = content[:start_pos + len(START_MARKER)] + new_content + content[end_pos:]
    content_changed = updated_content != content
    return content_changed, updated_content
  else:
    print("Index Markers not found in the file.")
    sys.exit(1)

def write_and_stage_readme(updated_content):
  with open(README_FILE, 'w') as file:
    file.write(updated_content)
  subprocess.run(GIT_COMMAND.split())

def main():
  all_media = get_all_media().strip().split('\n')
  index_by_medium, index_by_tier = parse_all_media(all_media)
  markdown_index_by_medium = pprint_markdown(index_by_medium)
  markdown_index_by_tier = pprint_markdown(index_by_tier)
  content = f'\n# Index by Medium\n{markdown_index_by_medium}\n# Index by Tier\n{markdown_index_by_tier}'
  content_changed, updated_content = replace_content_between_markers(content)
  if content_changed:
    write_and_stage_readme(updated_content)

main()
