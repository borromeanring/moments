# Protagonists
- Medium: Manga
- Title: [Haikyu!!](https://en.wikipedia.org/wiki/Haikyu!!)
- Chapter: [331](https://readhaikyuu.net/manga/haikyuu-chapter-331/)
- Tier: A

# We are the protagonists of the world.

![image](https://img.spoilerhat.com/img/?url=https://zjcdn.mangafox.me/store/manga/9904/TBD-331.0/compressed/d007.jpg)
![image](https://img.spoilerhat.com/img/?url=https://zjcdn.mangafox.me/store/manga/9904/TBD-331.0/compressed/d008.jpg)
![image](https://img.spoilerhat.com/img/?url=https://zjcdn.mangafox.me/store/manga/9904/TBD-331.0/compressed/d009.jpg)
