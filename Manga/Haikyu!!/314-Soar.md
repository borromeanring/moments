# Soar
- Medium: Manga
- Title: [Haikyu!!](https://en.wikipedia.org/wiki/Haikyu!!)
- Chapter: [314](https://readhaikyuu.net/manga/haikyuu-chapter-314/)
- Tier: S

# (Hinata breaks the birdcage)

![image](https://img.spoilerhat.com/img/?url=https://zjcdn.mangafox.me/store/manga/9904/TBD-314.0/compressed/g011.jpg)
![image](https://img.spoilerhat.com/img/?url=https://zjcdn.mangafox.me/store/manga/9904/TBD-314.0/compressed/g012.jpg)
![image](https://img.spoilerhat.com/img/?url=https://zjcdn.mangafox.me/store/manga/9904/TBD-314.0/compressed/g013.jpg)
![image](https://img.spoilerhat.com/img/?url=https://zjcdn.mangafox.me/store/manga/9904/TBD-314.0/compressed/g014.jpg)
![image](https://img.spoilerhat.com/img/?url=https://zjcdn.mangafox.me/store/manga/9904/TBD-314.0/compressed/g015.jpg)
