# Luffy's World
- Medium: Manga
- Title: [One Piece](https://en.wikipedia.org/wiki/One_Piece)
- Chapter: [1049](https://onepiecechapters.com/chapters/2268/one-piece-chapter-1049)
- Tier: S

# Mugiwara, what kind of world do you wish to make?

![image](https://cdn.onepiecechapters.com/file/CDN-M-A-N/op_tcb_1049_010.png)
![image](https://cdn.onepiecechapters.com/file/CDN-M-A-N/op_tcb_1049_015.png)
![image](https://cdn.onepiecechapters.com/file/CDN-M-A-N/op_tcb_1049_016.png)
![image](https://cdn.onepiecechapters.com/file/CDN-M-A-N/op_tcb_1049_017.png)
