# New Age
- Medium: Manga
- Title: [One Piece](https://en.wikipedia.org/wiki/One_Piece)
- Chapter: [1055](https://onepiecechapters.com/chapters/3923/one-piece-chapter-1055)
- Tier: S

# Does the new age frighten you that much?!

![image](https://cdn.onepiecechapters.com/file/CDN-M-A-N/onepiecetcb_1055_015.png)
![image](https://cdn.onepiecechapters.com/file/CDN-M-A-N/onepiecetcb_1055_016.png)
![image](https://cdn.onepiecechapters.com/file/CDN-M-A-N/onepiecetcb_1055_020.png)
![image](https://cdn.onepiecechapters.com/file/CDN-M-A-N/onepiecetcb_1055_021.png)
