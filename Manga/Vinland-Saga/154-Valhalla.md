# Valhalla
- Medium: Manga
- Title: [Vinland Saga](https://en.wikipedia.org/wiki/Vinland_Saga_(manga))
- Chapter: [154](https://ww2.readvinlandsaga.com/chapter/vinland-saga-chapter-154/)
- Tier: S

# There's no Valhalla. It's all a lie.

![image](https://cdn.readvinlandsaga.com/file/mangap/4741/10154000/8.jpg)
![image](https://cdn.readvinlandsaga.com/file/mangap/4741/10154000/9.jpg)
![image](https://cdn.readvinlandsaga.com/file/mangap/4741/10154000/10.jpg)
![image](https://cdn.readvinlandsaga.com/file/mangap/4741/10154000/11.jpg)
