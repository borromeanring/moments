# True Battle
- Medium: Manga
- Title: [Vinland Saga](https://en.wikipedia.org/wiki/Vinland_Saga_(manga))
- Chapters: [54](https://ww2.readvinlandsaga.com/chapter/vinland-saga-chapter-54/)
- Tier: S

# Be a true warrior, son of thors.

![image](https://cdn.readvinlandsaga.com/file/mangap/4741/10054000/9.jpg)
![image](https://cdn.readvinlandsaga.com/file/mangap/4741/10054000/10.jpg)
![image](https://cdn.readvinlandsaga.com/file/mangap/4741/10054000/11.jpg)
![image](https://cdn.readvinlandsaga.com/file/mangap/4741/10054000/12.jpg)
![image](https://cdn.readvinlandsaga.com/file/mangap/4741/10054000/13.jpg)
![image](https://cdn.readvinlandsaga.com/file/mangap/4741/10054000/14.jpg)
![image](https://cdn.readvinlandsaga.com/file/mangap/4741/10054000/15.jpg)
![image](https://cdn.readvinlandsaga.com/file/mangap/4741/10054000/16.jpg)
