# Saving Wales and Canute
- Medium: Manga
- Title: [Vinland Saga](https://en.wikipedia.org/wiki/Vinland_Saga_(manga))
- Chapters: [52](https://ww2.readvinlandsaga.com/chapter/vinland-saga-chapter-52/) and [53](https://ww2.readvinlandsaga.com/chapter/vinland-saga-chapter-53/)
- Tier: S

# When forced to choose between saving Wales and Canute, Askeladd sacrifices himself to save both.

![image](https://cdn.readvinlandsaga.com/file/mangap/4741/10052000/14.jpg)
![image](https://cdn.readvinlandsaga.com/file/mangap/4741/10052000/15.jpg)
![image](https://cdn.readvinlandsaga.com/file/mangap/4741/10052000/16.jpg)  
(omitted 8 pages)  
![image](https://cdn.readvinlandsaga.com/file/mangap/4741/10052000/25.jpg)
![image](https://cdn.readvinlandsaga.com/file/mangap/4741/10052000/26.jpg)  
(omitted 1 page)  
![image](https://cdn.readvinlandsaga.com/file/mangap/4741/10052000/28.jpg)  
(omitted 3 pages)  
![image](https://cdn.readvinlandsaga.com/file/mangap/4741/10052000/32.jpg)
(omitted the last page of chapter 52 and the first 13 pages of chapter 53)
![image](https://cdn.readvinlandsaga.com/file/mangap/4741/10053000/14.jpg)
