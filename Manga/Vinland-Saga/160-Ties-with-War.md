# Ties with War
- Medium: Manga
- Title: [Vinland Saga](https://en.wikipedia.org/wiki/Vinland_Saga_(manga))
- Chapter: [160](https://ww2.readvinlandsaga.com/chapter/vinland-saga-chapter-160/)
- Tier: B

# Nobody can cut ties with war. No matter where you are, if you've got three people, there will be war.

![image](https://cdn.readvinlandsaga.com/file/mangap/4741/10160000/8.jpg)
![image](https://cdn.readvinlandsaga.com/file/mangap/4741/10160000/9.jpg)
![image](https://cdn.readvinlandsaga.com/file/mangap/4741/10160000/10.jpg)
![image](https://cdn.readvinlandsaga.com/file/mangap/4741/10160000/11.jpg)
