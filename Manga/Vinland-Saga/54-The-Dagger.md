# The Dagger
- Medium: Manga
- Title: [Vinland Saga](https://en.wikipedia.org/wiki/Vinland_Saga_(manga))
- Chapters: [54](https://ww2.readvinlandsaga.com/chapter/vinland-saga-chapter-54/)
- Tier: S

# Thorfinn lets go of the dagger and sees a flashback

![image](https://cdn.readvinlandsaga.com/file/mangap/4741/10054000/32.jpg)
![image](https://cdn.readvinlandsaga.com/file/mangap/4741/10054000/33.jpg)
![image](https://cdn.readvinlandsaga.com/file/mangap/4741/10054000/34.jpg)
![image](https://cdn.readvinlandsaga.com/file/mangap/4741/10054000/35.jpg)
