# Being Empty
- Medium: Manga
- Title: [Vinland Saga](https://en.wikipedia.org/wiki/Vinland_Saga_(manga))
- Chapters: [68](https://ww2.readvinlandsaga.com/chapter/vinland-saga-chapter-68/)
- Tier: S

# Being empty means anything can fit inside you. If you want to be reborn, empty's the best way to be.

![image](https://cdn.readvinlandsaga.com/file/mangap/4741/10068000/17.jpg)
![image](https://cdn.readvinlandsaga.com/file/mangap/4741/10068000/18.jpg)
![image](https://cdn.readvinlandsaga.com/file/mangap/4741/10068000/19.jpg)
![image](https://cdn.readvinlandsaga.com/file/mangap/4741/10068000/20.jpg)  
(omitted 4 pages)  
![image](https://cdn.readvinlandsaga.com/file/mangap/4741/10068000/25.jpg)
![image](https://cdn.readvinlandsaga.com/file/mangap/4741/10068000/26.jpg)
