# The Dead
- Medium: Manga
- Title: [Vagabond](https://en.wikipedia.org/wiki/Vagabond_(manga))
- Chapter: [306](https://manga1s.com/manga/vagabond/chapter-306)
- Tier: A

# The dead do not return.

![image](https://img.m1cdn.xyz//5/61a6e64bfd198c46d493eec5/61c1d272ad40dadecea96086/028.jpg)
![image](https://img.m1cdn.xyz//5/61a6e64bfd198c46d493eec5/61c1d272ad40dadecea96086/029.jpg)
![image](https://img.m1cdn.xyz//5/61a6e64bfd198c46d493eec5/61c1d272ad40dadecea96086/030.jpg)
![image](https://img.m1cdn.xyz//5/61a6e64bfd198c46d493eec5/61c1d272ad40dadecea96086/031.jpg)
![image](https://img.m1cdn.xyz//5/61a6e64bfd198c46d493eec5/61c1d272ad40dadecea96086/032.jpg)
