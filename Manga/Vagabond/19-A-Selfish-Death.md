# A Selfish Death
- Medium: Manga
- Title: [Vagabond](https://en.wikipedia.org/wiki/Vagabond_(manga))
- Chapter: [19](https://manga1s.com/manga/vagabond/chapter-19)
- Tier: A

# So you want to die honorably? A warrior's death? Hmph! You're being selfish. Each and every person you killed had his own life.

![image](https://img.m1cdn.xyz//5/61a6e64bfd198c46d493eec5/61c1d272ad40dadecea961a5/002.jpg)
(omitted 1 page)  
![image](https://img.m1cdn.xyz//5/61a6e64bfd198c46d493eec5/61c1d272ad40dadecea961a5/004.jpg)
![image](https://img.m1cdn.xyz//5/61a6e64bfd198c46d493eec5/61c1d272ad40dadecea961a5/005.jpg)
![image](https://img.m1cdn.xyz//5/61a6e64bfd198c46d493eec5/61c1d272ad40dadecea961a5/006.jpg)
![image](https://img.m1cdn.xyz//5/61a6e64bfd198c46d493eec5/61c1d272ad40dadecea961a5/007.jpg)
![image](https://img.m1cdn.xyz//5/61a6e64bfd198c46d493eec5/61c1d272ad40dadecea961a5/008.jpg)
![image](https://img.m1cdn.xyz//5/61a6e64bfd198c46d493eec5/61c1d272ad40dadecea961a5/009.jpg)
![image](https://img.m1cdn.xyz//5/61a6e64bfd198c46d493eec5/61c1d272ad40dadecea961a5/010.jpg)
