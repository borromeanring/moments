# The Value Of Life
- Medium: Manga
- Title: [Vagabond](https://en.wikipedia.org/wiki/Vagabond_(manga))
- Chapter: [317](https://manga1s.com/manga/vagabond/chapter-317)
- Tier: S

# "Life has no value!" Truly right. It has no value. If you think only of yourself, it has no value. "Why was I born?" The reason you are here... is because someone supported your life. The reason you are here... is to support lives. 

![image](https://img.m1cdn.xyz//5/61a6e64bfd198c46d493eec5/61c1d272ad40dadecea9607b/018.jpg)  
(omitted 1 page)  
![image](https://img.m1cdn.xyz//5/61a6e64bfd198c46d493eec5/61c1d272ad40dadecea9607b/020.jpg)
![image](https://img.m1cdn.xyz//5/61a6e64bfd198c46d493eec5/61c1d272ad40dadecea9607b/021.jpg)
![image](https://img.m1cdn.xyz//5/61a6e64bfd198c46d493eec5/61c1d272ad40dadecea9607b/022.jpg)
![image](https://img.m1cdn.xyz//5/61a6e64bfd198c46d493eec5/61c1d272ad40dadecea9607b/023.jpg)
![image](https://img.m1cdn.xyz//5/61a6e64bfd198c46d493eec5/61c1d272ad40dadecea9607b/024.jpg)
![image](https://img.m1cdn.xyz//5/61a6e64bfd198c46d493eec5/61c1d272ad40dadecea9607b/025.jpg)  
(omitted 2 pages)  
![image](https://img.m1cdn.xyz//5/61a6e64bfd198c46d493eec5/61c1d272ad40dadecea9607b/028.jpg)
![image](https://img.m1cdn.xyz//5/61a6e64bfd198c46d493eec5/61c1d272ad40dadecea9607b/029.jpg)
