# Truly Strong People
- Medium: Manga
- Title: [Vagabond](https://en.wikipedia.org/wiki/Vagabond_(manga))
- Chapter: [224](https://manga1s.com/manga/vagabond/chapter-224)
- Tier: S

# You've become kinder. You're getting stronger, I see. All truly strong people are kind.

![image](https://img.m1cdn.xyz//5/61a6e64bfd198c46d493eec5/61c1d272ad40dadecea960d8/016.jpg)
![image](https://img.m1cdn.xyz//5/61a6e64bfd198c46d493eec5/61c1d272ad40dadecea960d8/017.jpg)
