# Being Free
- Medium: Manga
- Title: [Vagabond](https://en.wikipedia.org/wiki/Vagabond_(manga))
- Chapter: [317](https://manga1s.com/manga/vagabond/chapter-317)
- Tier: S

# If you don't go against how life should be... you're already completely... free. 

![image](https://img.m1cdn.xyz//5/61a6e64bfd198c46d493eec5/61c1d272ad40dadecea9607b/030.jpg)
![image](https://img.m1cdn.xyz//5/61a6e64bfd198c46d493eec5/61c1d272ad40dadecea9607b/031.jpg)
