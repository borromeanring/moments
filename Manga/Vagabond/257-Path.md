# Path
- Medium: Manga
- Title: [Vagabond](https://en.wikipedia.org/wiki/Vagabond_(manga))
- Chapter: [257](https://manga1s.com/manga/vagabond/chapter-257)
- Tier: B

# Each path born is by heaven perfectly decided and at once perfectly free. As long as everything is left up to heaven.

![image](https://img.m1cdn.xyz//5/61a6e64bfd198c46d493eec5/61c1d272ad40dadecea960b7/010.jpg)  
(omitted 1 page)  
![image](https://img.m1cdn.xyz//5/61a6e64bfd198c46d493eec5/61c1d272ad40dadecea960b7/012.jpg)
![image](https://img.m1cdn.xyz//5/61a6e64bfd198c46d493eec5/61c1d272ad40dadecea960b7/013.jpg)
