# The Meaning Of Strength
- Medium: Manga
- Title: [Vagabond](https://en.wikipedia.org/wiki/Vagabond_(manga))
- Chapter: [311](https://manga1s.com/manga/vagabond/chapter-311)
- Tier: S

# What is the meaning of 'Strength'? It's to have a mind that doesn't sway... while continuing to move and change.

![image](https://img.m1cdn.xyz//5/61a6e64bfd198c46d493eec5/61c1d272ad40dadecea96081/031.jpg)
![image](https://img.m1cdn.xyz//5/61a6e64bfd198c46d493eec5/61c1d272ad40dadecea96081/032.jpg)
