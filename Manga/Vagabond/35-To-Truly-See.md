# To Truly See
- Medium: Manga
- Title: [Vagabond](https://en.wikipedia.org/wiki/Vagabond_(manga))
- Chapter: [35](https://manga1s.com/manga/vagabond/chapter-35)
- Tier: S

# Don't be preoccupied with a single spot. See everything in it's entirety... effortlessly. That is what it means to truly "see".

![image](https://img.m1cdn.xyz//5/61a6e64bfd198c46d493eec5/61c1d272ad40dadecea96195/018.jpg)
![image](https://img.m1cdn.xyz//5/61a6e64bfd198c46d493eec5/61c1d272ad40dadecea96195/019.jpg)
