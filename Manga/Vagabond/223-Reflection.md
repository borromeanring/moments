# Reflection
- Medium: Manga
- Title: [Vagabond](https://en.wikipedia.org/wiki/Vagabond_(manga))
- Chapter: [223](https://manga1s.com/manga/vagabond/chapter-223)
- Tier: S

# You weren't looking at me last night. You were looking at the 'me' you created in the few years we hadn't seen each other. The 'me' in your mind.

![image](https://img.m1cdn.xyz//5/61a6e64bfd198c46d493eec5/61c1d272ad40dadecea960d9/004.jpg)
![image](https://img.m1cdn.xyz//5/61a6e64bfd198c46d493eec5/61c1d272ad40dadecea960d9/005.jpg)
![image](https://img.m1cdn.xyz//5/61a6e64bfd198c46d493eec5/61c1d272ad40dadecea960d9/006.jpg)
![image](https://img.m1cdn.xyz//5/61a6e64bfd198c46d493eec5/61c1d272ad40dadecea960d9/007.jpg)
![image](https://img.m1cdn.xyz//5/61a6e64bfd198c46d493eec5/61c1d272ad40dadecea960d9/008.jpg)
![image](https://img.m1cdn.xyz//5/61a6e64bfd198c46d493eec5/61c1d272ad40dadecea960d9/009.jpg)
![image](https://img.m1cdn.xyz//5/61a6e64bfd198c46d493eec5/61c1d272ad40dadecea960d9/010.jpg)
![image](https://img.m1cdn.xyz//5/61a6e64bfd198c46d493eec5/61c1d272ad40dadecea960d9/011.jpg)
