# To Experience
- Medium: Manga
- Title: [Vagabond](https://en.wikipedia.org/wiki/Vagabond_(manga))
- Chapter: [76](https://manga1s.com/manga/vagabond/chapter-76)
- Tier: B

# So many things in this world... cannot be expressed with words. Some things can not be explained... they must be experienced.

![image](https://img.m1cdn.xyz//5/61a6e64bfd198c46d493eec5/61c1d272ad40dadecea9616c/015.jpg)
