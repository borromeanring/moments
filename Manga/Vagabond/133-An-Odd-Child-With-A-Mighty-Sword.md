# An Odd Child With A Mighty Sword
- Medium: Manga
- Title: [Vagabond](https://en.wikipedia.org/wiki/Vagabond_(manga))
- Chapter: [133](https://manga1s.com/manga/vagabond/chapter-133)
- Tier: B

# I see that sword invigorates you. However, it can also be the death of you.

![image](https://img.m1cdn.xyz//5/61a6e64bfd198c46d493eec5/61c1d272ad40dadecea96133/010.jpg)  
(omitted 1 page)  
![image](https://img.m1cdn.xyz//5/61a6e64bfd198c46d493eec5/61c1d272ad40dadecea96133/012.jpg)  
(omitted 1 page)  
![image](https://img.m1cdn.xyz//5/61a6e64bfd198c46d493eec5/61c1d272ad40dadecea96133/014.jpg)
![image](https://img.m1cdn.xyz//5/61a6e64bfd198c46d493eec5/61c1d272ad40dadecea96133/015.jpg)
![image](https://img.m1cdn.xyz//5/61a6e64bfd198c46d493eec5/61c1d272ad40dadecea96133/016.jpg)
![image](https://img.m1cdn.xyz//5/61a6e64bfd198c46d493eec5/61c1d272ad40dadecea96133/017.jpg)
![image](https://img.m1cdn.xyz//5/61a6e64bfd198c46d493eec5/61c1d272ad40dadecea96133/018.jpg)
![image](https://img.m1cdn.xyz//5/61a6e64bfd198c46d493eec5/61c1d272ad40dadecea96133/019.jpg)
![image](https://img.m1cdn.xyz//5/61a6e64bfd198c46d493eec5/61c1d272ad40dadecea96133/020.jpg)
